<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

#DATABASE SETTINGS
const DB_HOST = '127.0.0.1';
const DB_PORT = '3306';
const DB_DATABASE = 'pool_tournament';
const DB_USERNAME = 'root';
const DB_PASSWORD = 'password';

#DEMO DATA
const DEMO_DEFAULT_PASSWORD = 'admin';

#GAME SETTINGS
const POINTS_PER_VICTORY = 3;
const POINTS_PER_LOSE = 1;

#DIRS
DEFINE('ROOT_DIR', dirname(__FILE__));
const CLASSES_DIR = ROOT_DIR . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR;
const COMMANDS_DIR = ROOT_DIR . DIRECTORY_SEPARATOR . 'Commands' . DIRECTORY_SEPARATOR;
const LOGGER_DIR = ROOT_DIR . DIRECTORY_SEPARATOR . 'Logger' . DIRECTORY_SEPARATOR;
const VENDOR_DIR = ROOT_DIR . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR;
