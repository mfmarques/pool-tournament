<?php

require_once('requires.php');

//CORS
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');

$data = json_decode(file_get_contents('php://input'));

if (isset($data) && isset($data->action)) {
    switch ($data->action) {
        case 'getMatches':
        case 'getMatchDetail':
        case 'createMatch':
            (new Matches())->{$data->action}($data);
            break;
        case 'getRanking':
            (new Ranking())->getRanking($data);
            break;
        case 'signUp':
        case 'login':
        case 'getUserDetail':
            (new User())->{$data->action}($data);
            break;
        case 'getUsers':
            (new User())->getUsers();
            break;
        default:
            (new Base())->error('Unknown request');
    }
} else {
    (new Base())->error('Unknown request');
}
