<?php

class Ranking extends Base
{
    /**
     * Get ranking
     * 
     * @param mixed $data
     */
    public function getRanking($data)
    {
        $db = Database::getInstance();

        try {
            $sql = "SELECT * FROM ranking_view";

            if (isset($data->query) && (!empty($data->query) || !is_null($data->query))) {
                $sql .= " WHERE name LIKE :query";
            }

            $stmt = $db->prepare($sql);

            if (isset($data->query) && (!empty($data->query) || !is_null($data->query))) {
                $stmt->execute([':query' => '%' . $data->query . '%']);
            } else {
                $stmt->execute();
            }

            return $this->success($stmt->fetchAll(PDO::FETCH_ASSOC));
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }
    }
}
