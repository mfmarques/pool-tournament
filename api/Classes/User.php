<?php

use Ramsey\Uuid\Uuid;

class User extends Base
{
    /**
     * Get all users.
     */
    public function getUsers()
    {
        $db = Database::getInstance();

        try {
            $users = $db->prepare("SELECT * FROM users_view u ORDER BY name ASC;");
            $users->execute();

            return $this->success($users->fetchAll(PDO::FETCH_ASSOC));
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }
    }

    /**
     * Get user detail.
     * 
     * @param mixed $data
     */
    public function getUserDetail($data)
    {
        $this->validatePayload(['id'], $data);

        $db = Database::getInstance();

        try {
            $sql = "SELECT * FROM users_view u WHERE u.id = :userId";

            $detail = $db->prepare($sql);
            $detail->execute([':userId' => $data->id]);

            $matchesSql = "SELECT * FROM matches_view 
                WHERE player1_id = :userId OR player2_id = :userId
                ORDER BY created_at DESC";

            $matches = $db->prepare($matchesSql);
            $matches->execute([':userId' => $data->id]);

            if ($detail->rowCount()) {
                return $this->success(array_merge($detail->fetch(PDO::FETCH_ASSOC), ['matches' => $matches->fetchAll(PDO::FETCH_ASSOC)]));
            } else {
                return $this->error('User not found');
            }
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }
    }

    /**
     * Login
     * 
     * @param mixed $data
     */
    public function login($data)
    {
        $this->validatePayload(['username', 'password'], $data);

        $db = Database::getInstance();

        try {
            $stmt = $db->prepare("SELECT * FROM users WHERE username = :username");
            $stmt->execute([':username' => $data->username]);

            if ($res = $stmt->fetch()) {
                if (password_verify($data->password, $res['password'])) {
                    return $this->success(array_merge(['api_key' => $res['api_key'], 'name' => $res['first_name'] . ' ' . $res['last_name']]));
                } else {
                    return $this->error('Username or password is invalid');
                }
            } else {
                return $this->error('Username or password is invalid');
            }
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }
    }

    /**
     * Sign up.
     * 
     * @param mixed $data
     */
    public function signUp($data)
    {
        $this->validatePayload(['username', 'first_name', 'last_name', 'password', 'password_confirmation'], $data);

        if ($data->password !== $data->password_confirmation) {
            return $this->error('Password and password confirmation must be the same');
        }

        $db = Database::getInstance();

        try {
            $stmt = $db->prepare("SELECT * FROM users WHERE username = :username");
            $stmt->execute([':username' => $data->username]);

            if ($stmt->rowCount()) {
                return $this->error('Username is already registered');
            }
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }

        try {
            $db->beginTransaction();

            $stmt = $db->prepare("INSERT INTO users (id,username,first_name,last_name,api_key,password) VALUES (:id,:username,:first_name,:last_name,:api_key,:password)");
            $stmt->execute([
                ':id' => Uuid::uuid4(),
                ':username' => $data->username,
                ':first_name' => $data->first_name,
                ':last_name' => $data->last_name,
                ':api_key' => Uuid::uuid4(),
                ':password' => password_hash($data->password, PASSWORD_DEFAULT)
            ]);

            $db->commit();

            if ($stmt->rowCount()) {
                return $this->success('Your account has been successfully created');
            } else {
                return $this->error("Couldn't create your account please try again later");
            }
        } catch (PDOException $e) {
            $db->rollback();
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }
    }
}
