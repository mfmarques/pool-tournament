<?php

class Matches extends Base
{
    /**
     * Create match.
     *
     * @param $data
     */
    public function createMatch($data)
    {
        $this->validatePayload(['player1_id', 'player2_id', 'winner', 'balls_left'], $data);

        if ($data->winner <> '1' && $data->winner <> '2') {
            return $this->error('The winner must be the Player 1 or Player 2');
        }

        if ($data->player1_id === $data->player2_id) {
            return $this->error('Player 1 and Player 2 must be different');
        }

        $db = Database::getInstance();

        // Check if the player1 id and player 2 id exists
        try {
            $stmt = $db->prepare("SELECT * FROM users WHERE id = :player1_id OR id = :player2_id");
            $stmt->execute([':player1_id' => $data->player1_id, ':player2_id' => $data->player2_id]);

            if ($stmt->rowCount() != 2) {
                return $this->error('Please check again the provided player ids.');
            }
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }

        // Friend A can only play once against friend B
        try {
            $stmt = $db->prepare("SELECT * FROM matches WHERE (player1_id = :player1_id AND player2_id = :player2_id) OR
                (player1_id = :player2_id AND player2_id = :player1_id)");

            $stmt->execute([':player1_id' => $data->player1_id, ':player2_id' => $data->player2_id]);

            if ($stmt->rowCount()) {
                return $this->error('Player 1 can only play once against Player 2.');
            }
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }

        try {
            $db->beginTransaction();

            $stmt = $db->prepare("INSERT INTO matches (player1_id,player2_id,winner_id,balls_left) VALUES (:player1_id,:player2_id,:winner_id,:balls_left)");
            $stmt->execute([
                ':player1_id' => $data->player1_id,
                ':player2_id' => $data->player2_id,
                ':winner_id' => $data->winner == '1' ? $data->player1_id : $data->player2_id,
                ':balls_left' => $data->balls_left
            ]);

            $createdMatchId = $db->lastInsertId();
            $db->commit();

            if ($stmt->rowCount()) {
                return $this->success($createdMatchId);
            } else {
                return $this->error("Couldn't create the match please try again later");
            }
        } catch (PDOException $e) {
            $db->rollback();
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }
    }

    /**
     * Get matches.
     *
     * @param mixed $data
     */
    public function getMatches($data)
    {
        $db = Database::getInstance();

        try {
            $sql = "SELECT * FROM matches_view";

            if (isset($data->query) && (!empty($data->query) || !is_null($data->query))) {
                $sql .= " WHERE player1_name LIKE :query OR player2_name LIKE :query";
            }

            $sql .= " ORDER BY created_at DESC";
            $stmt = $db->prepare($sql);

            if (isset($data->query) && (!empty($data->query) || !is_null($data->query))) {
                $stmt->execute([':query' => '%' . $data->query . '%']);
            } else {
                $stmt->execute();
            }

            return $this->success($stmt->fetchAll(PDO::FETCH_ASSOC));
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }
    }

    /**
     * Get match detail.
     * 
     * @param mixed $data
     */
    public function getMatchDetail($data)
    {
        $this->validatePayload(['id'], $data);

        $db = Database::getInstance();

        try {
            $sql = "SELECT m.* FROM matches_view m WHERE m.id = :matchId";

            $stmt = $db->prepare($sql);
            $stmt->execute([':matchId' => $data->id]);

            if ($stmt->rowCount()) {
                return $this->success($stmt->fetch(PDO::FETCH_ASSOC));
            } else {
                return $this->error('Match not found');
            }
        } catch (PDOException $e) {
            return $this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]);
        }
    }
}
