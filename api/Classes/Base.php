<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Base
{
    /**
     * API success response.
     * 
     * @param mixed $data
     * @return void
     */
    public function success($data): void
    {
        echo json_encode(array_merge(['success' => true], ['data' => $data]));
    }

    /**
     * API error response.
     * 
     * @param string $message
     * @param array $context
     * 
     * @return void
     */
    public function error(string $message, array $context = []): void
    {
        if ($context) {
            $log = new Logger('pool-tournament');
            $log->pushHandler(new StreamHandler(LOGGER_DIR . 'pool-tournament.log', Logger::CRITICAL));
            $log->critical(json_encode($context));
        }

        echo json_encode(['success' => false, 'message' => $message]);
    }

    /**
     * Validate form fields.
     * 
     * @param array $required Required fields
     * @param mixed $form
     * 
     * @return void
     */
    public function validatePayload(array $required, $form)
    {
        foreach ($required as $field) {
            if (!isset($form->{$field}) || (isset($form->{$field}) && empty($form->{$field}) || is_null($form->{$field}))) {
                die($this->error("{$field} is required"));
            }
        }
    }

    /**
     * Command question.
     * 
     * @param string $question
     * @return string
     */
    public function commandQuestion(string $question): string
    {
        $this->commandLog($question, 'warning');
        $answer = rtrim(fgets(STDIN));

        while ($answer <> 'y' && $answer <> 'n') {
            $this->commandLog($question, 'warning');
            $answer = rtrim(fgets(STDIN));
        }

        return $answer;
    }

    /**
     * Create log with colors (Used in Commands)
     * 
     * @param string $str
     * @param string $type
     * @return void
     */
    public function commandLog(string $str, string $type)
    {
        switch ($type) {
            case 'error':
                echo "\033[31m$str \033[0m\n";
                break;
            case 'success':
                echo "\033[32m$str \033[0m\n";
                break;
            case 'warning':
                echo "\033[33m$str \033[0m\n";
                break;
            case 'info':
                echo "\033[36m$str \033[0m\n";
                break;
        }
    }
}
