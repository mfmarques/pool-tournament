<?php

class Database extends Base
{
    /**
     * @var PDO
     */
    protected static $instance;

    /**
     * Database constructor
     * Singleton class
     */
    private function __construct()
    {
        try {
            self::$instance = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_DATABASE, DB_USERNAME, DB_PASSWORD);
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die($this->error('Some error occurred please try again later', [__FILE__, __FUNCTION__, $e]));
        }
    }

    /**
     * Get database instance
     * 
     * @return PDO
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            return (new self())::$instance;
        }

        return self::$instance;
    }
}
