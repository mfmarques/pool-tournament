<?php

require_once(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'requires.php');

class CreateDatabase extends Base
{
    /**
     * CreateDatabase constructor.
     */
    public function __construct()
    {
        $answer = $this->commandQuestion('This will delete all your current data. Are you sure that you want to create the database? (y/n)');

        if ($answer == 'y') {
            $db = Database::getInstance();

            foreach (get_class_methods($this) as $method) {
                if (preg_match('(generate|Table|View)', $method) === 1) {
                    $table = str_replace(['generate', 'Table', 'View'], ['', '', ''], $method);
                    try {
                        $this->commandLog("Preparing to generate {$table}.", 'info');
                        $db->exec($this->$method());
                        $this->commandLog("Generated {$table} successfully", 'success');
                    } catch (PDOException $e) {
                        $this->commandLog("Error creating {$table} - {$e->getMessage()}", 'error');
                    }
                }
            }
            $this->commandLog('Created database successfully', 'success');
        } else {
            $this->commandLog('Stopped the database creation from running successfully', 'success');
        }
    }

    /**
     * Generate users table.
     * 
     * @return string
     */
    private function generateUsersTable(): string
    {
        return "DROP TABLE IF EXISTS `users`;
            CREATE TABLE `users` (
                `id` char(36) NOT NULL,
                `username` varchar(255) NOT NULL,
                `first_name` varchar(255) NOT NULL,
                `last_name` varchar(255) NOT NULL,
                `avatar` varchar(255) DEFAULT NULL,
                `email` varchar(255) DEFAULT NULL,
                `date_of_birth` date DEFAULT NULL,
                `api_key` varchar(255) NOT NULL,
                `password` varchar(255) NOT NULL,
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`),
                UNIQUE KEY `username_UNIQUE` (`username`),
                UNIQUE KEY `api_key_UNIQUE` (`api_key`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
          
        CREATE OR REPLACE VIEW users_view AS
        SELECT id, username, first_name, last_name, CONCAT(users.first_name,' ',users.last_name) as name, avatar, email, date_of_birth, created_at
        FROM users;";
    }


    /**
     * Generate matches table.
     * 
     * @return string
     */
    private function generateMatchesTable(): string
    {
        return "DROP TABLE IF EXISTS `matches`;
            CREATE TABLE `matches` (
                `id` int NOT NULL AUTO_INCREMENT,
                `player1_id` char(36) NOT NULL,
                `player2_id` char(36) NOT NULL,
                `winner_id` char(36) NOT NULL,
                `balls_left` tinyint NOT NULL,
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
            
        CREATE OR REPLACE VIEW matches_view AS
        SELECT m.*, p1.name as player1_name, p2.name as player2_name, p1.avatar as player1_avatar, p2.avatar as player2_avatar
        FROM matches m
        JOIN users_view p1 ON m.player1_id = p1.id
        JOIN users_view p2 ON m.player2_id = p2.id;";
    }

    /**
     * Generate ranking view.
     * 
     * @return string
     */
    private function generateRankingView(): string
    {
        return "CREATE OR REPLACE VIEW ranking_view AS
        SELECT u.*,
            (SELECT COUNT(*) FROM matches WHERE player1_id = u.id OR player2_id = u.id) as matches,
            (SELECT COUNT(*) FROM matches WHERE winner_id = u.id) as victories,
            ((SELECT matches) - (SELECT victories)) as loses,
            ((SELECT victories) * " . POINTS_PER_VICTORY . ") + ((SELECT loses) * " . POINTS_PER_LOSE . ") as points,
            (SELECT SUM(balls_left) FROM matches WHERE player1_id = u.id OR player2_id = u.id) as sum_balls_left
        FROM users_view u
        ORDER BY points DESC, sum_balls_left ASC;";
    }
}

new CreateDatabase();
