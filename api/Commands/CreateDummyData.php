<?php

require_once(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'requires.php');

use Ramsey\Uuid\Uuid;

class CreateDummyData extends Base
{
    /**
     * CreateDummyData constructor.
     */
    public function __construct()
    {
        $answer = $this->commandQuestion('This will delete all your current data. Are you sure that you want to refresh your database? (y/n)');

        if ($answer == 'y') {
            $db = Database::getInstance();

            foreach (get_class_methods($this) as $method) {
                if (preg_match('(seed|Table)', $method) === 1) {
                    $table = str_replace(['seed', 'Table'], ['', ''], $method);
                    try {
                        $this->commandLog("Preparing to seed {$table} table.", 'info');
                        $db->exec('TRUNCATE ' . strtolower($table));
                        $this->commandLog("Truncated {$table} table successfully", 'info');
                        $this->commandLog("Start seeding with dummy data", 'info');
                        $this->$method();
                        $this->commandLog("Seeded {$table} table successfully", 'success');
                    } catch (PDOException $e) {
                        $this->commandLog("Error seeding {$table} table - {$e->getMessage()}", 'error');
                    }
                }
            }
            $this->commandLog('Seeded the database successfully', 'success');
        } else {
            $this->commandLog('Stopped the database seeder from running successfully', 'success');
        }
    }

    /**
     * Seed users table.
     * 
     * @return void
     */
    private function seedUsersTable(): void
    {
        $db = Database::getInstance();
        $faker = Faker\Factory::create('pt_PT');
        $faker->addProvider(new Ottaviano\Faker\Gravatar($faker));

        for ($i = 0; $i <= 100; $i++) {
            try {
                $db->beginTransaction();

                $userName = $faker->userName();

                $stmt = $db->prepare("INSERT INTO users 
                    (id,username,first_name,last_name,avatar,email,date_of_birth,api_key,password) VALUES 
                    (:id,:username,:first_name,:last_name,:avatar,:email,:date_of_birth,:api_key,:password)
                ");
                $stmt->execute([
                    ':id' => Uuid::uuid4(),
                    ':username' => $userName,
                    ':first_name' => $faker->firstName(),
                    ':last_name' => $faker->lastName(),
                    ':avatar' => $faker->gravatarUrl(),
                    ':email' => $faker->safeEmail(),
                    ':date_of_birth' => $faker->date(),
                    ':api_key' => Uuid::uuid4(),
                    ':password' => password_hash(DEMO_DEFAULT_PASSWORD, PASSWORD_DEFAULT)
                ]);

                $db->commit();
                $this->commandLog('Created user: ' . $userName, 'success');
            } catch (PDOException $e) {
                $db->rollback();
                $this->commandLog("Error creating a dummy user - {$e->getMessage()}", 'error');
            }
        }
    }


    /**
     * Seed matches table.
     * 
     * @return void
     */
    private function seedMatchesTable(): void
    {
        $db = Database::getInstance();

        for ($i = 0; $i <= 100; $i++) {
            try {
                $db->beginTransaction();

                $stmt = $db->prepare("SELECT id FROM users ORDER BY RAND() LIMIT 0,2");
                $stmt->execute();
                $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

                $stmt = $db->prepare("INSERT INTO matches (player1_id,player2_id,winner_id,balls_left) VALUES (:player1_id,:player2_id,:winner_id,:balls_left)");
                $stmt->execute([
                    ':player1_id' => $res[0]['id'],
                    ':player2_id' => $res[1]['id'],
                    ':winner_id' => $res[array_rand($res)]['id'],
                    ':balls_left' => rand(1, 10),
                ]);

                $db->commit();
            } catch (PDOException $e) {
                $db->rollback();
                $this->commandLog("Error creating a dummy match - {$e->getMessage()}", 'error');
            }
        }
    }
}

new CreateDummyData();
