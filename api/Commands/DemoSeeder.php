<?php

require_once(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'requires.php');

class DemoSeeder extends Base
{
    /**
     * DemoSeeder constructor.
     */
    public function __construct()
    {
        require_once(COMMANDS_DIR . 'CreateDatabase.php');
        require_once(COMMANDS_DIR . 'CreateDummyData.php');
    }
}

new DemoSeeder();
