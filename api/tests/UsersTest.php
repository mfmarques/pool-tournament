<?php 

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'requires.php');

final class UsersTest extends TestCase
{
    public function testCanGetUsers(): void
    {
        $db = Database::getInstance();
        $stmt = $db->prepare("SELECT * FROM users;");
        $this->assertTrue($stmt->execute());
    }

    public function testCannotGetUsersFromInvalidTable(): void
    {
        $this->expectException(PDOException::class);

        $db = Database::getInstance();
        $stmt = $db->prepare("SELECT * FROM userrrrrrrrrrrrrrrs;");
        $stmt->execute();
    }
}