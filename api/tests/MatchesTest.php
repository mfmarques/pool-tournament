<?php 

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'requires.php');

final class MatchesTest extends TestCase
{
    public function testCanGetMatches(): void
    {
        $db = Database::getInstance();
        $stmt = $db->prepare("SELECT * FROM matches;");
        $this->assertTrue($stmt->execute());
    }

    public function testCannotGetMatchesFromInvalidTable(): void
    {
        $this->expectException(PDOException::class);

        $db = Database::getInstance();
        $stmt = $db->prepare("SELECT * FROM matchesssssssssssss;");
        $stmt->execute();
    }
}