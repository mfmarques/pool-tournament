<?php 

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'requires.php');

final class RankingTest extends TestCase
{
    public function testCanGetRanking(): void
    {
        $db = Database::getInstance();
        $stmt = $db->prepare("SELECT * FROM ranking_view;");
        $this->assertTrue($stmt->execute());
    }

    public function testCannotGetRankingFromInvalidTable(): void
    {
        $this->expectException(PDOException::class);

        $db = Database::getInstance();
        $stmt = $db->prepare("SELECT * FROM rankinggggGGGggg_view;");
        $stmt->execute();
    }
}