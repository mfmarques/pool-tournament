<?php

require_once('configs.php');

require_once(VENDOR_DIR . 'autoload.php');

require_once(CLASSES_DIR . 'Base.php');
require_once(CLASSES_DIR . 'Database.php');
require_once(CLASSES_DIR . 'User.php');
require_once(CLASSES_DIR . 'Matches.php');
require_once(CLASSES_DIR . 'Ranking.php');