# Pool tournament by mmarques

## Project structure
```
Simple authentication 

Homepage
    Block with ranking
        Ordered by points (first) and fewer balls left (second)
    Block with list of matches
        Search by friend name
        Link to match detail
        Link to friend detail

Match detail page
    Match info

Friend detail page
    Friend info
    Block with list of games

Submission page
    Form to submit a match result between two friends
```

## Project setup
```
npm install
cd api && composer install
sudo chmod 777 -Rf Logger/
```

## Change environment variables and run the migrations
```
Change your database and game settings in 'api/configs.php'
Run in terminal: php api/Commands/DemoSeeder.php
Change the api endpoint in '.env'
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Run api tests
```
cd api && ./vendor/bin/phpunit tests
```