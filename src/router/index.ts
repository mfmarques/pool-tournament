import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "home",
        component: Home
    },
    {
        path: "/sign-up",
        name: "signup",
        component: () => import("../views/Signup.vue")
    },
    {
        path: "/login",
        name: "login",
        component: () => import("../views/Login.vue")
    },
    {
        path: "/match/:matchId",
        name: "match",
        component: () => import("../views/Match.vue")
    },
    {
        path: "/create-match",
        name: "create-match",
        component: () => import("../views/CreateMatch.vue")
    },
    {
        path: "/user/:userId",
        name: "user",
        component: () => import("../views/User.vue")
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
