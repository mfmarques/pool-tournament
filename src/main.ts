import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

require("mdb-ui-kit/css/mdb.min.css");

createApp(App)
    .use(router)
    .use(Toast)
    .mount("#app");
